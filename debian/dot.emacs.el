;;; .emacs --- sample of ~/.emacs for MHC  -*-mode: emacs-lisp-*-

(when (locate-library "mhc")
  (setq mhc-use-wide-scope 14)
;;;  (setq mhc-use-wide-scope 'week)
;;;  (setq mhc-use-wide-scope 'wide)
;;;  (setq mhc-use-wide-scope nil)
  (global-set-key "\C-cI" 'mhc-import)
  (eval-after-load "mhc-draft"
    (quote
     (progn
       (defun my-mhc-draft-finish-and-exit ()
	 (interactive)
	 (let ((noninteractive t))
	   (mhc-draft-finish)))
       (define-key mhc-draft-mode-map "\C-c\C-z" 'my-mhc-draft-finish-and-exit))))
  ;;
  )

;;; .emacs ends here
